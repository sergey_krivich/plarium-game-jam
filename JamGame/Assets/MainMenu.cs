﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

  public Image Back;

  private bool _started;

  void Update()
  {
    if (!_started)
    {
      Back.color = Color.Lerp(Back.color, new Color(0,0,0,0), Time.deltaTime * 3f);
    }
  }

  IEnumerator DoStart()
  {
    float elapsed = 0f;
    while (elapsed < 1.6f)
    {
      elapsed += Time.deltaTime;
      Back.color = Color.Lerp(Back.color, Color.black, Time.deltaTime * 2f);
      yield return null;
    }

    SceneManager.LoadScene("game");
  }

  public void PressOk()
  {
    _started = true;
    StartCoroutine(DoStart());
  }
}
