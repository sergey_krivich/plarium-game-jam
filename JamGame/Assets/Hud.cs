﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Hud : MonoBehaviour
{

  public Slider PlayerHp;
  public Slider RiffHp;

  public Image Char1;
  public Image Char2;
  public Image Char3;

  public Image BlackBack;

  public Image LevelCompleteBack;
  public Text LevelCompleteText;
  public Text LevelStartText;

  public Text EnemiesCounter;

  public Image ForegroundSelfHp;
  public Image ForegroundOtherHp;

  private bool _restartGameStarted;

  private float _lastHp;
  private float _lastHpRiff;

  private Color _hpStartSelf;
  private Color _hpStartRiff;

  void Awake()
  {
    LevelCompleteBack.color = Color.Lerp(LevelCompleteBack.color, new Color(0, 0, 0, 0), Time.deltaTime);
    foreach (var i in LevelCompleteBack.GetComponentsInChildren<Image>())
    {
      if (i == LevelCompleteBack)
        continue;
      i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
    }

    foreach (var i in LevelCompleteBack.GetComponentsInChildren<Text>())
    {
      if (i == LevelCompleteBack)
        continue;
      i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
    }

    LevelStartText.transform.localPosition = new Vector3(-1000, 0, 0);
    BlackBack.color = new Color(0,0,0,1);

    _hpStartSelf = ForegroundSelfHp.color;
    _hpStartRiff = ForegroundOtherHp.color;
  }

  void Start()
  {
    StartCoroutine(PlayLevelStart());
    _lastHp = PlayerControl.I.HP;
  }

  void Update()
  {
    if (!_restartGameStarted)
    {
      if (Time.deltaTime < 0.2f)
        BlackBack.color = Color.Lerp(BlackBack.color, new Color(0,0,0,0), Time.deltaTime * 1f);
    }

    PlayerHp.value = PlayerControl.I.HP/PlayerControl.I.MaxHP;
    RiffHp.value = Riff.I.HP/Riff.I.MaxHP;

    if (_lastHp != PlayerControl.I.HP)
    {
      _lastHp = PlayerControl.I.HP;
      ForegroundSelfHp.color = Color.Lerp(ForegroundSelfHp.color, Color.white, 0.7f);
    }
    else
    {
      ForegroundSelfHp.color = Color.Lerp(ForegroundSelfHp.color, _hpStartSelf, Time.deltaTime*5);
    }

    if (_lastHpRiff != Riff.I.HP)
    {
      _lastHpRiff = Riff.I.HP;
      ForegroundOtherHp.color = Color.Lerp(ForegroundOtherHp.color, Color.white, 0.7f);
    }
    else
    {
      ForegroundOtherHp.color = Color.Lerp(ForegroundOtherHp.color, _hpStartRiff, Time.deltaTime*5);
    }

    if (PlayerControl.I.State == PlayerCharState.Eat)
    {
      Char1.color = new Color(1, 1, 1, 0.85f);
      Char2.color = Color.green;
      Char3.color = new Color(1, 1, 1, 0.85f);
    }
    if (PlayerControl.I.State == PlayerCharState.Bombs)
    {
      Char1.color = new Color(1, 1, 1, 0.85f);
      Char2.color = new Color(1, 1, 1, 0.85f);
      Char3.color = Color.green;
    }

    if (PlayerControl.I.State == PlayerCharState.Attract)
    {
      Char1.color = Color.green;
      Char2.color = new Color(1, 1, 1, 0.85f);
      Char3.color = new Color(1, 1, 1, 0.85f);
    }

    EnemiesCounter.text = (EnemyControl.AliveEnemies + GameManager.I.EnemiesLeft).ToString();
    UpdateFinish();
  }

  private bool wasLEvelFinished = false;
  private void UpdateFinish()
  {
    if (_restartGameStarted)
    {
      return;
    }

    if (GameManager.I.IsLevelFinished)
    {
      if (!wasLEvelFinished)
      {
        AudioManager.I.Music.Stop();
        AudioManager.I.Music.volume = 0f;
        AudioManager.I.Play(AudioManager.I.Win);
        wasLEvelFinished = true;
      }

      LevelCompleteBack.color = Color.Lerp(LevelCompleteBack.color, new Color(0, 0, 0, 0.5f), Time.deltaTime*2);
      foreach (var i in LevelCompleteBack.GetComponentsInChildren<Image>())
      {
        if (i == LevelCompleteBack)
          continue;

        var alpha = i.color.a;
        alpha = Mathf.Lerp(alpha, 1f, Time.deltaTime);
        i.color = new Color(i.color.r, i.color.g, i.color.b, alpha);
      }

      foreach (var i in LevelCompleteBack.GetComponentsInChildren<Text>())
      {
        if (i == LevelCompleteBack)
          continue;

        var alpha = i.color.a;
        alpha = Mathf.Lerp(alpha, 1f, Time.deltaTime*2);
        i.color = new Color(i.color.r, i.color.g, i.color.b, alpha);
      }

      LevelCompleteText.transform.localPosition = Vector3.Lerp(LevelCompleteText.transform.localPosition, new Vector3(),
        Time.deltaTime*5f);
    }
    else
    {
      wasLEvelFinished = false;
      if (PlayerControl.I.IsDead || Riff.I.IsDead)
      {
        _restartGameStarted = true;
        StopAllCoroutines();
        StartCoroutine(PlayLevelStart("You failed!", false));
        StartCoroutine(RestartGame());
        AudioManager.I.Music.volume = 0f;
        AudioManager.I.Music.Stop();
        AudioManager.I.Play(AudioManager.I.Lose);
      }
      else
      {
        LevelCompleteBack.color = Color.Lerp(LevelCompleteBack.color, new Color(0, 0, 0, 0f), Time.deltaTime);
        foreach (var i in LevelCompleteBack.GetComponentsInChildren<Image>())
        {
          if (i == LevelCompleteBack)
            continue;

          var alpha = i.color.a;
          alpha = Mathf.Lerp(alpha, 0f, Time.deltaTime*3);
          i.color = new Color(i.color.r, i.color.g, i.color.b, alpha);
        }

        foreach (var i in LevelCompleteBack.GetComponentsInChildren<Text>())
        {
          if (i == LevelCompleteBack)
            continue;

          var alpha = i.color.a;
          alpha = Mathf.Lerp(alpha, 0f, Time.deltaTime*3);
          i.color = new Color(i.color.r, i.color.g, i.color.b, alpha);
        }

        LevelCompleteText.transform.localPosition = Vector3.Lerp(LevelCompleteText.transform.localPosition,
          new Vector3(-1000, 0, 0), Time.deltaTime*5f);
      }
    }
  }

  public void NextLevelClick()
  {
    if (GameManager.I.IsLevelFinished)
    {
      GameManager.I.NextLevel();
      StartCoroutine(PlayLevelStart());
      AudioManager.I.Music.volume = 0.1f;
      AudioManager.I.PlayMusic();
    }
  }

  private IEnumerator PlayLevelStart(string text = "", bool goBack = true)
  {
    yield return new WaitForSeconds(0.5f);

    if (string.IsNullOrEmpty(text))
      LevelStartText.text = "Level " + (GameManager.I.CurrentLevel + 1);
    else
    {
      LevelStartText.text = text;
    }

    float start = 0;
    while (start < 2f)
    {
      start += Time.deltaTime;
      LevelStartText.transform.localPosition = Vector3.Lerp(LevelStartText.transform.localPosition, new Vector3(),
        Time.deltaTime*5f);
      yield return null;
    }
    start = 0;
    if (goBack)
    {
      while (start < 3f)
      {
        start += Time.deltaTime;
        LevelStartText.transform.localPosition = Vector3.Lerp(LevelStartText.transform.localPosition,
          new Vector3(-1000, 0, 0), Time.deltaTime*5f);
        yield return null;
      }
    }
  }


  private IEnumerator RestartGame()
  {
    yield return new WaitForSeconds(1.5f);

    float start = 0;
    while (start < 2f)
    {
      start += Time.deltaTime;
      BlackBack.color = Color.Lerp(BlackBack.color, Color.black, Time.deltaTime*3f);
      yield return null;
    }

    EnemyControl.AliveEnemies = 0;
    SceneManager.LoadScene("game", LoadSceneMode.Single);
  }


  public void SelectMeduza()
  {
    PlayerControl.I.SetAtrractState();
  }
  public void SelectAkula()
  {
    PlayerControl.I.SetEatStaete();
  }
  public void SelectBombs()
  {
    PlayerControl.I.SetBombsState();
  }
}
