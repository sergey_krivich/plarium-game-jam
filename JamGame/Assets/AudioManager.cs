﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
  public AudioClip BombExplosion;
  public AudioClip BombPlant;
  public AudioClip Eat;
  public AudioClip KickPers;
  public AudioClip KickRiff;
  public AudioClip Switch;
  public AudioClip Lose;
  public AudioClip Win;

  public AudioSource Music;

  public static AudioManager I;
  public void Awake()
  {
    I = this;
  }


  public void PlayMusic()
  {
    Music.Play();
  }

  public void Play(AudioClip clip)
  {
    AudioSource.PlayClipAtPoint(clip, PlayerControl.I != null ? PlayerControl.I.CC.Center.position : new Vector3());
  }
}
