﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{
  public Image Back;

	IEnumerator Start () 
  {
		yield return new WaitForSeconds(4.5f);
	  float elapsed = 0f;
	  while (elapsed < 2f)
	  {
	    elapsed += Time.deltaTime;
	    Back.color = Color.Lerp(Back.color, Color.black, Time.deltaTime*2f);
	    yield return null;
	  }
    
    SceneManager.LoadScene("mainmenu");
	}
}
