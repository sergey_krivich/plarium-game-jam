﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
  public GameObject Target;
  public Rigidbody2D TargetRigidbody2D;
  public float Speed = 5f;

  public Camera Cam;
  public static CameraControl I;

  public float VelocityOffset = 1f;
  public float SizeMin = 3f;
  public float SizeMax = 7f;
  public float SizeAdd = 1f;

  public Vector3 Offset = new Vector3(0, 0, -10);

  private Vector3 _position;

  private List<Shaker> _shakers = new List<Shaker>();

  private class Shaker
  {
    public float _shakeAmount;
    public float _shakeSpeed;
    public float ShakeCalm = 1f;
    public float ShakeCalmSpeed = 1f;
  }

  //mapX, mapY is size of background image
  public float mapX = 61.4f;
  public float mapY = 41.4f;
  private float minX;
  private float maxX;
  private float minY;
  private float maxY;


  public void Awake()
  {
    I = this;
    Cam = GetComponent<Camera>();
    _position = transform.position;
  }

  void Update()
  {
    if (Target == null || TargetRigidbody2D == null)
    {
      return;
    }

    _position = Vector3.Lerp(_position,
      Target.transform.position + Offset + (Vector3) TargetRigidbody2D.velocity*VelocityOffset, Time.deltaTime*Speed);

    var sizeOffset = TargetRigidbody2D.velocity.magnitude * SizeAdd;
    sizeOffset = Mathf.Clamp(sizeOffset, SizeMin, SizeMax);

    Cam.orthographicSize = Mathf.Lerp(Cam.orthographicSize, sizeOffset, Time.deltaTime*3f);

    var shakeVector = new Vector3();
    foreach (var shaker in _shakers.ToList())
    {
      shakeVector += new Vector3(0, Mathf.Sin(Time.time*shaker._shakeSpeed)*shaker._shakeAmount);
      shaker._shakeSpeed = Mathf.Lerp(shaker._shakeSpeed, 0f, Time.deltaTime * shaker.ShakeCalmSpeed);
      shaker._shakeAmount = Mathf.Lerp(shaker._shakeAmount, 0f, Time.deltaTime * shaker.ShakeCalm);

      if (shaker._shakeAmount <= 0.01f)
      {
        _shakers.Remove(shaker);
      }
    }


    var vertExtent = Cam.orthographicSize;
    var horzExtent = vertExtent * Screen.width / Screen.height;

    // Calculations assume map is position at the origin
    minX = horzExtent - mapX / 2.0f;
    maxX = mapX / 2.0f - horzExtent;
    minY = vertExtent - mapY / 2.0f;
    maxY = mapY / 2.0f - vertExtent;

    Vector3 v3 = _position;
    v3.x = Mathf.Clamp(v3.x, minX, maxX);
    v3.y = Mathf.Clamp(v3.y, minY, maxY);
    _position = v3;

    transform.position = _position + shakeVector;


  }


  public void ShakeAdd(float amount, float speed, float ShakeCalmSpeed, float ShakeCalm)
  {
    _shakers.Add(new Shaker
    {
      _shakeAmount =  amount,
      _shakeSpeed = speed,
      ShakeCalmSpeed = ShakeCalmSpeed,
      ShakeCalm = ShakeCalm
    });
  }
}
