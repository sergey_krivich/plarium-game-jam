﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelSettings : MonoBehaviour 
{
  [Serializable]
  public class Level
  {
    public int EnemiesCount;

    public float FirstEnemyChance;
    public float SecondEnemyChance;
    public float ThirdEnemyChance;

    public float SpawnDelay;


    public float WaveDuration = 10f;
    public float TimeBetweenWaves = 4f;
  }

  public List<Level> Levels;
}
