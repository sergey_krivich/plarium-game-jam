﻿using System.Linq;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{
  [Header("Detection")]
  public float DetectionRadius = 4f;
  public float DetectionRadiusAttracts = 6f;
  private float _detectionRadius = 4f;
  public float StopDistance = 1f;

  [Header("Acc")]
  public float AccIdle = 1f;
  public float AccDetected = 2f;
  public float AccAttracts = 3;

  [Header("MaxSpeeds")]
  public float MaxSpeed = 2;
  public float MaxSpeedDetected = 3;
  public float MaxSpeedAttracts = 4;


  [Header("Other")] 
  public float MaxHP = 1;
  public float HP = 1;
  public float Damage = 5;
  public float DamageToRiff = 5;
  public float PlayerHpRestorationOnEat = 5;

  [HideInInspector]
  public CharacterControl CC;
  private Vector2? _lastPlayerPosition;

  private Vector2 _targetPosition;
  private Rigidbody2D _rigidbody;
  private CharacterControl _characterControl;

  private float _stunTimer;
  private bool _followingPlayer;
  public Animator AnimatorMain;

  public bool IsDead { get; set; }
  public static int AliveEnemies { get; set; }

  void Awake ()
	{
	  CC = GetComponent<CharacterControl>();
	  _rigidbody = GetComponent<Rigidbody2D>();
    _characterControl = GetComponent<CharacterControl>();
	  _detectionRadius = DetectionRadius;
    AliveEnemies++;
	}
	
	void Update ()
	{
    if(PlayerControl.I.IsDead)
      return;

	  CheckDistance();
	  Move();

    if (_followingPlayer)
    {
      if (PlayerControl.I.State == PlayerCharState.Attract)
      {
        _detectionRadius = DetectionRadiusAttracts;
        CC.MaxSpeed = MaxSpeedAttracts;
        CC.Acc = AccAttracts;
      }
      else
      {
        _detectionRadius = DetectionRadius;
        CC.MaxSpeed = MaxSpeedDetected;
        CC.Acc = AccDetected;
      }
    }
    else
    {
      _detectionRadius = DetectionRadius;
      CC.MaxSpeed = MaxSpeed;
      CC.Acc = AccIdle;
    }

    if (_stunTimer > 0)
	    _stunTimer -= Time.deltaTime;
	}

  private void CheckDistance()
  {
    var distance = Vector2.Distance(PlayerControl.I.transform.position, transform.position);
    if (distance < DetectionRadius)
    {
      _followingPlayer = true;
      _lastPlayerPosition = PlayerControl.I.CC.Center.transform.position;
    }
  }

  private void Move()
  {
    UpdateTarget();
    MoveToTarget();
  }

  private void MoveToTarget()
  {
    var direction = (_targetPosition - (Vector2) transform.position).normalized;
    var distanceTolastPosition = Vector2.Distance(_targetPosition, transform.position);

    var distance = Vector2.Distance(PlayerControl.I.transform.position, transform.position);
    if (distance > DetectionRadius && distanceTolastPosition < StopDistance)
    {
      _followingPlayer = false;
      _lastPlayerPosition = null;
    }

    if (_stunTimer > 0f)
    {
      direction = new Vector2();
    }

    if (_lastPlayerPosition != null || distanceTolastPosition > StopDistance)
    {
      CC.Move(direction);
    }
  }

  private void UpdateTarget()
  {
    if (_lastPlayerPosition != null)
    {
      _targetPosition = _lastPlayerPosition.Value;
    }
    else
    {
      var distance = Vector2.Distance(Riff.I.transform.position, transform.position);
      if (distance > StopDistance)
      {
        _targetPosition = Riff.I.transform.position;
      }
    }
  }

  private void OnCollisionEnter2D(Collision2D collision)
  {
    if(IsDead)
      return;

    if (collision.collider.CompareTag("Player"))
    {
      PlayAttack(collision);
      PlayerControl.I.TakeDamage(Damage);
    }


    if (collision.collider.CompareTag("Riff"))
    {
      PlayAttack(collision);
      collision.collider.GetComponentInParent<Riff>().TakeDamage(DamageToRiff);
      Instantiate(ParticlesHolder.I.BloodHit, collision.contacts.FirstOrDefault().point, Quaternion.identity);
    }
  }

  private void PlayAttack(Collision2D collision)
  {
    var point = collision.contacts.FirstOrDefault();

    var dir = ((Vector2) transform.position - point.point).normalized;
    _rigidbody.velocity = -dir*_characterControl.MaxSpeed;
    _stunTimer = 1f;
  }

  void OnDrawGizmosSelected()
  {
    Gizmos.color = new Color(0, 1, 0, 0.2f);
    Gizmos.DrawSphere(transform.position, DetectionRadiusAttracts);
    Gizmos.color = new Color(1,1,1,0.4f);
    Gizmos.DrawSphere(transform.position, DetectionRadius);
  }

  public void TakeDamage(float damage)
  {
    if (IsDead)
      return;

    HP -= damage;
    if (HP <= 0)
    {
      Die();
    }
  }

  private void Die()
  {
    if(IsDead)
      return;

    IsDead = true;
    AliveEnemies--;
    Destroy(this);

    CC.Move(new Vector2());
    CC.ChangeRotation = false;
    _rigidbody.mass = _rigidbody.mass/10;

    var deadBody = gameObject.AddComponent<DeadBody>();
    deadBody.PlayerHpRestorationOnEat = PlayerHpRestorationOnEat;

    AnimatorMain.SetBool("IsDead", true);
    var r = Instantiate(ParticlesHolder.I.EnemyDead, CC.Center.position + new Vector3(0,0,0.01f), Quaternion.Euler(0, 90, 0));
    r.transform.SetParent(transform, true);
    r.transform.position = CC.Center.position + new Vector3(0, 0, 0.01f);


    var r2 = Instantiate(ParticlesHolder.I.BloodContinious, CC.Center.position + new Vector3(0, 0, 0.01f), Quaternion.Euler(0, 90, 0));
    r2.transform.SetParent(transform, true);
    r2.transform.position = CC.Center.position + new Vector3(0, 0, 0.01f);
    deadBody.Particle = r2;

  }
}

