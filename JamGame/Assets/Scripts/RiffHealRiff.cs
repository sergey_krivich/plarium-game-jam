﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RiffHealRiff : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
  public float Speed;
  private bool _heal;

  public AudioSource Source;

  void Start()
  {
  }

  void Update()
  {
    if (_heal)
    {
      var amount = Time.deltaTime * Speed;
      if (Riff.I.HP + amount < Riff.I.MaxHP)
      {
        Riff.I.HP += amount;
        PlayerControl.I.TakeDamage(amount, false);
      }

    }
    GetComponent<Image>().color = _heal ? Color.green : Color.white;

    if(!_heal)
      Source.Stop();
    else
    {
      if (!Source.isPlaying)
        Source.Play();
    }
  }

  public void OnPointerDown(PointerEventData eventData)
  {
    _heal = true;
  }

  public void OnPointerUp(PointerEventData eventData)
  {
    _heal = false;
  }
}
