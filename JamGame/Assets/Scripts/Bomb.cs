﻿using UnityEngine;

public class Bomb : MonoBehaviour
{
  public float DurationToExpl;
  public float ExplosionRadius;
  private float _elapsed;

  public GameObject Explosion;
  void Update()
  {
    _elapsed += Time.deltaTime;
    if (_elapsed > DurationToExpl)
    {
      Explode();
    }
  }

  private void Explode()
  {
    var enemies = FindObjectsOfType < EnemyControl>();

    foreach (var hitt in enemies)
    {
      var enemy = hitt.gameObject.GetComponent<EnemyControl>();
      if (enemy && !enemy.IsDead && Vector2.Distance(enemy.CC.Center.transform.position, transform.position) <= ExplosionRadius)
      {
        enemy.TakeDamage(1);
      }
    }

    if (PlayerControl.I != null)
    {
      var d = Vector3.Distance(transform.position, PlayerControl.I.CC.Center.position);
      d = Mathf.Clamp(d, 1f, 5f);
      CameraControl.I.ShakeAdd(0.14f/Mathf.Sqrt(d), 1.6f, 4, 3);
    }

    AudioManager.I.Play(AudioManager.I.BombExplosion);
    Instantiate(Explosion, transform.position, Quaternion.Euler(90,0,0));
    Destroy(gameObject);
  }

  void OnDrawGizmosSelected()
  {
    Gizmos.color = new Color(1, 0, 0.2f, 0.2f);
    Gizmos.DrawSphere(transform.position, ExplosionRadius);
  }
}
