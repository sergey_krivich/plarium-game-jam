﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RiffHealSelf : MonoBehaviour,IPointerDownHandler, IPointerUpHandler
{
  public float Speed;
  private bool _heal;
  public AudioSource Source;

	void Start () {
		
	}
	
	void Update () {
	  if (_heal)
	  {
	    var amount = Time.deltaTime*Speed;

	    if (PlayerControl.I.HP + amount < PlayerControl.I.MaxHP)
	    {
	      Riff.I.HP -= amount;
	      PlayerControl.I.HP += amount;
	    }
	  }

    GetComponent<Image>().color = _heal ? Color.green : Color.white;

    if (!_heal)
      Source.Stop();
    else
    {
      if (!Source.isPlaying)
        Source.Play();
    }
	}

  public void OnPointerDown(PointerEventData eventData)
  {
    _heal = true;
  }

  public void OnPointerUp(PointerEventData eventData)
  {
    _heal = false;
  }
}
