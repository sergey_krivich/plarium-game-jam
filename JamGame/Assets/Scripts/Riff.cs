﻿using UnityEngine;

public class Riff : MonoBehaviour
{

  public static Riff I;

  public float MaxHP;
  public float HP;

  public Material Material;

  public SpriteRenderer[] Sprites;
  public bool IsDead {
    get { return HP <= 0; }
  }

  void Awake()
  {
    I = this;

    foreach (var spriteRenderer in Sprites)
    {
      spriteRenderer.material = new Material(Material);
    }
  }

  void Update()
  {
    foreach (var spriteRenderer in Sprites)
    {
      spriteRenderer.color = Color.Lerp(spriteRenderer.color, Color.white, Time.deltaTime);
    }
    UpdateVisual();
  }

  public void TakeDamage(float damageToRiff)
  {
    HP -= damageToRiff;
    AudioManager.I.Play(AudioManager.I.KickRiff);
    UpdateVisual();

    foreach (var spriteRenderer in Sprites)
    {
      spriteRenderer.color = Color.Lerp(spriteRenderer.color, Color.red, 0.6f);
    }
  }

  private void UpdateVisual()
  {
    var hpLeftPercent = HP/MaxHP;

    var step = 1f/(Sprites.Length);
    for (int i = 0; i < Sprites.Length; i++)
    {
      var t = (i + 1)*step;
      var b = 1f - ((i + 1)*step);
      float amount = Mathf.Clamp01((b - hpLeftPercent) / t);
      Sprites[i].material.SetFloat("_Power", amount);
    }
  }
}
