﻿using System;
using UnityEngine;

public class CharacterControl : MonoBehaviour
{
  [Header("Speed")]
  public float Acc = 5;
  public float MaxSpeed = 5;
  public float Friction = 3;

  [Header("Rotation")]
  public float RotSmoothment = 3;
  public float AngleRot = 3;


  [Header("Other")]
  public GameObject Graphics;
  public bool ChangeRotation = true;

  private Rigidbody2D _rigidbody;
  private Vector2 _direction;
  private Vector2 _lastNonZero;

  public Animator _animator;

  public Transform Center;

  private float _currentTail;

  private float _targetAngle;

  public SkinnedMeshRenderer Rednerer;


	void Awake ()
	{
    _rigidbody = GetComponent<Rigidbody2D>();
	}

  void Update()
  {
    UpdateTail();

   
    var sr = GetComponent<SpriteRenderer>();
    if (Rednerer != null && sr != null && sr.sprite != null && Rednerer.material != null)
    {
      foreach (var rednererMaterial in Rednerer.materials)
      {
        rednererMaterial.mainTexture = sr.sprite.texture;
      }
    }

  }

  private void UpdateTail()
  {
    Vector3 forwardA = transform.localRotation*Vector3.right;
    Vector3 forwardB = _direction;
    if (_direction.magnitude <= float.Epsilon)
    {
      forwardB = transform.right;
    }

    var signedAngle = Vector3.Angle(forwardA, forwardB);
    var cross = Vector3.Cross(forwardA, forwardB);
    if (cross.z < 0)
      signedAngle = -signedAngle;

    var targetTail = Mathf.Clamp(signedAngle/AngleRot, -1, 1);
    _currentTail = Mathf.Lerp(_currentTail, (1f + targetTail)/2f, Time.deltaTime*RotSmoothment);
    _animator.SetFloat("Blend", _currentTail);

    Debug.DrawLine(transform.position, transform.position + forwardA, Color.green);
    Debug.DrawLine(transform.position, transform.position + forwardB, Color.red);
    Debug.DrawLine(transform.position, transform.position + cross, Color.blue);
  }

  void FixedUpdate()
  {

    if (_direction.magnitude <= float.Epsilon)
    {
      _rigidbody.velocity = Vector2.Lerp(_rigidbody.velocity, new Vector2(), Time.fixedDeltaTime*Friction);
    }
    else
    {
      _rigidbody.velocity += _direction * Time.fixedDeltaTime * Acc;
    }
    _rigidbody.velocity = Vector2.ClampMagnitude(_rigidbody.velocity, MaxSpeed);

    var dir = _rigidbody.velocity.normalized;
    dir = _lastNonZero.normalized;
    //if (dir.magnitude <= float.Epsilon)
    //{
    //  dir = _lastNonZero.normalized;
    //}
    var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    _targetAngle = Mathf.LerpAngle(_targetAngle, angle, Time.fixedDeltaTime*AngleRot);

    if (ChangeRotation)
    {
      _rigidbody.MoveRotation(_targetAngle);
    }
  }

  public void Move(Vector2 direction)
  {
    _direction = direction;

    if (direction.magnitude > 0)
    {
      _lastNonZero = direction;
    }
  }
}
