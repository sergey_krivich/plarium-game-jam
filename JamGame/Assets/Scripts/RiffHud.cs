﻿using UnityEngine;
using UnityEngine.UI;

public class RiffHud : MonoBehaviour
{
  public Image[] Images;
  public Text[] Text;

  public void Update()
  {
    foreach (var image in Images)
    {
      if (PlayerControl.I != null)
      {
        image.enabled = Vector2.Distance(PlayerControl.I.CC.Center.position, transform.position) < 2;
      }
    }

    foreach (var image in Text)
    {
      if (PlayerControl.I != null)
      {
        image.enabled = Vector2.Distance(PlayerControl.I.CC.Center.position, transform.position) < 2;
      }
    }
  }
}
