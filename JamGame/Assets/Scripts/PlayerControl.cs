﻿using UnityEngine;

public enum PlayerCharState
{
  Bombs = 0,
  Eat = 1,
  Attract = 2,
  None = 40
}

public class PlayerControl : MonoBehaviour
{
  [HideInInspector]
  public CharacterControl CC;
  public static PlayerControl I;

  public PlayerCharState State;

  [Header("Acc")]
  public float AccAttract = 1f;
  public float AccEat = 2f;
  public float AccBombs = 3;

  [Header("MaxSpeeds")]
  public float MaxSpeedAttract = 2;
  public float MaxSpeedEat = 3;
  public float MaxSpeedBombs = 4;

  [Header("Damages")]
  public float DamageAttract = 1f;
  public float DamageEat = 1.5f;
  public float DamageMultBombs = 2f;

  [Header("Costs")]
  public float BombCost = 5f;
  public float ChangeCost = 5f;

  [Header("Hp")]
  public float MaxHP = 100;
  public float HP = 100;

  [Header("Other")] 
  public float PutBombDelay = 1.5f;


  [Header("GR")]
  public Material MainMat;
  public Material AddMat;
  public GameObject BloodHit;
  public Transform BombSpawnPoint;

  public Animator AnimatorMain;

  public GameObject Bomb;

  private float _bombElapsed;
  public bool IsDead { get; set; }

  private float _damageMult;

  private Color _targetColor = Color.white;

  private SkinnedMeshRenderer _meshRenderer;

  private bool playFx;
  private float _switchDelay;



  void Awake ()
	{
	  HP = MaxHP;
    I = this;
    CC = GetComponent<CharacterControl>();
	  _meshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
    State = PlayerCharState.None;

	}

  void Start()
  {
    SetBombsState();
    playFx = true;
  }

  void Update()
  {
    if(_switchDelay > 0)
      _switchDelay -= Time.deltaTime;

    UpdateMove();
    UpdateState();
    UpdateBomb();
    UpdateMAt();
  }

  private void UpdateMAt()
  {
    _targetColor = Color.Lerp(_targetColor, Color.white, Time.deltaTime*3);

    if (State == PlayerCharState.Attract)
    {
      foreach (var m in _meshRenderer.materials)
      {
        if (m.shader.name == "Sprites/AdditiveSpriteChar")
          m.SetColor("_Color",
            new Color(Mathf.Abs(Mathf.Sin(Time.time*7)*2),
              Mathf.Abs(Mathf.Cos(Time.time*5)),
              Mathf.Sin((Time.time + 1)*7)*2,
              1f));
        else
          m.SetColor("_Color", new Color(0.1f, 0.1f, 0.1f, 1f));
      }
    }
    else
    {
      foreach (var m in _meshRenderer.materials)
      {
        m.SetColor("_Color", _targetColor);
      }
    }
  }

  private void UpdateBomb()
  {
    if (_bombElapsed > 0)
    {
      _bombElapsed -= Time.deltaTime;
    }

    if (Vector2.Distance(CC.Center.position, Riff.I.transform.position) < 1.6f)
    {
      return;
    }

    if (GameManager.I.IsLevelFinished)
    {
      CC.Move(new Vector2());
      return;
    }

    if (State == PlayerCharState.Bombs)
    {
      if (_bombElapsed <= 0f)
      {
        var bomb = Instantiate(Bomb, BombSpawnPoint.position, Quaternion.identity);
        _bombElapsed = PutBombDelay;
        AudioManager.I.Play(AudioManager.I.BombPlant);
      }
    }
  }

  private void UpdateMove()
  {
    if (GameManager.I.IsLevelFinished)
    {
      CC.Move(new Vector2());
      return;
    }

    if (Input.GetMouseButton(0))
    {
      var worldPos = CameraControl.I.Cam.ScreenToWorldPoint(Input.mousePosition);
      var direction = (worldPos - CC.Center.position).normalized;

      CC.Move(direction);
    }
    else
    {
      CC.Move(new Vector2());
    }
  }

  private void UpdateState()
  {
    if (GameManager.I.IsLevelFinished || _switchDelay > 0)
      return;

    if (Input.GetKeyDown(KeyCode.Alpha1))
    {
      SetAtrractState();
    }
    else if (Input.GetKeyDown(KeyCode.Alpha2))
    {
      SetEatStaete();
    }
    else if (Input.GetKeyDown(KeyCode.Alpha3))
    {
      SetBombsState();
    }

    AnimatorMain.SetInteger("state",(int)State);
  }

  public void SetBombsState()
  {
    if (State == PlayerCharState.Bombs)
      return;

    CC.MaxSpeed = MaxSpeedBombs;
    CC.Acc = AccBombs;
    _damageMult = DamageMultBombs;
    State = PlayerCharState.Bombs;
    _meshRenderer.materials = new[] {MainMat};
    _switchDelay = 0.5f;

    SwitchFx();
  }

  private void SwitchFx()
  {
    if (playFx)
    {
      AudioManager.I.Play(AudioManager.I.Switch);
      Instantiate(ParticlesHolder.I.ChangeForm, CC.Center, false);
    }
  }

  public void SetEatStaete()
  {
    if (State == PlayerCharState.Eat)
      return;
    CC.MaxSpeed = MaxSpeedEat;
    CC.Acc = AccEat;
    State = PlayerCharState.Eat;
    _damageMult = DamageEat;
    _meshRenderer.materials = new[] {MainMat};
    _switchDelay = 0.5f;
    SwitchFx();

  }

  public void SetAtrractState()
  {
    if (State == PlayerCharState.Attract)
      return;

    CC.MaxSpeed = MaxSpeedAttract;
    CC.Acc = AccAttract;
    State = PlayerCharState.Attract;
    _damageMult = DamageAttract;
    _meshRenderer.materials = new[] {MainMat, AddMat};
    _switchDelay = 0.5f;
    SwitchFx();

  }

  public void TakeDamage(float damage, bool fx = true)
  {
    HP -= damage* _damageMult;
    _targetColor = Color.Lerp(_targetColor, Color.red, 0.1f*damage*_damageMult);
    if (fx)
    {
      Instantiate(BloodHit, CC.Center.position, Quaternion.identity);
      CameraControl.I.ShakeAdd(0.15f, 1.4f, 2, 2);
      AudioManager.I.Play(AudioManager.I.KickPers);
    }
    if (HP <= 0)
    {
      Die();
    }

    ClampHp();
  }

  private void OnCollisionEnter2D(Collision2D collision)
  {
    var deadBody = collision.collider.GetComponent<DeadBody>();
    if (deadBody && State == PlayerCharState.Eat)
    {
      HP += deadBody.PlayerHpRestorationOnEat;
      ClampHp();

      CameraControl.I.ShakeAdd(0.2f, 1f, 5f,4f);
      AudioManager.I.Play(AudioManager.I.Eat);

      CC.GetComponent<Rigidbody2D>().velocity *= 0.9f;
      var r = Instantiate(ParticlesHolder.I.EnemyEaten, transform.position + new Vector3(0, 0, 0.01f), Quaternion.Euler(0, 90, 0));

      deadBody.Particle.transform.parent = null;
      var dis = deadBody.Particle.AddComponent<DestroyInSeconds>();
      deadBody.Particle.GetComponent<ParticleSystem>().Stop();
      dis.Time = 3.0f;

      Destroy(deadBody.gameObject);
    }
  }

  private void ClampHp()
  {
    HP = Mathf.Clamp(HP, 0, MaxHP);
  }

  private void Die()
  {
    if (IsDead)
      return;
    IsDead = true;
    Destroy(gameObject);
  }
}
