﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  [HideInInspector]
  public int CurrentLevel = -1;
  [HideInInspector]
  public int EnemiesLeft;

  public GameObject Enemy1;
  public GameObject Enemy2;
  public GameObject Enemy3;






  public bool IsLevelFinished
  {
    get { return EnemiesLeft <= 0 && EnemyControl.AliveEnemies == 0; }
  }

  private float _spawnDelay;

  private LevelSettings _levels;

  private EnemySpawner[] _spawners;

  private bool _waveWait;
  private float _waveElapased;

  public static GameManager I;
  private void Awake()
  {
    _levels = GetComponent<LevelSettings>();
    _spawners = FindObjectsOfType<EnemySpawner>();
    I = this;
  }

  void Start()
  {
    AudioManager.I.PlayMusic();
    NextLevel();
  }

  void Update()
  {
    if (!_waveWait)
      _spawnDelay += Time.deltaTime;

    _waveElapased += Time.deltaTime;
    if (!_waveWait)
    {
      if (_waveElapased >= Level.WaveDuration)
      {
        _waveElapased = 0f;
        _waveWait = true;
      }
    }
    else
    {
      if (_waveElapased >= Level.TimeBetweenWaves)
      {
        _waveWait = false;
        _waveElapased = 0f;
      }
    }

    if (EnemiesLeft > 0)
    {
      if (_spawnDelay >= Level.SpawnDelay)
      {
        Spawn();
      }
    }
  }

  private void Spawn()
  {
    EnemiesLeft--;
    _spawnDelay = 0f;

    int spawnerIndex = Random.Range(0, _spawners.Length);
    var spawner = _spawners[spawnerIndex];
    var enemyTypeRes = Random.Range(0f, 1f);

    var enemyPrefab = GetRandomEnemty(enemyTypeRes);
    if (enemyPrefab == null)
    {
      Debug.LogError("no prefabenemy");
    }
    var go = GameObject.Instantiate(enemyPrefab);
    go.transform.position = spawner.transform.position;

    var r = Instantiate(ParticlesHolder.I.SpawnEnemy);
    r.transform.position = spawner.transform.position + new Vector3(0,0,-1);
  }

  private GameObject GetRandomEnemty(float enemyTypeRes)
  {
    var enemyPrefab = Enemy1;
    var dict = new Dictionary<float, GameObject>();

    dict[Level.FirstEnemyChance] = Enemy1;
    dict[Level.SecondEnemyChance] = Enemy2;
    dict[Level.ThirdEnemyChance] = Enemy3;

    foreach (var source in dict.OrderBy(k => k.Key))
    {
      if (source.Key > enemyTypeRes)
      {
        enemyPrefab = source.Value;
      }
    }
    return enemyPrefab;
  }

  public void NextLevel()
  {
    CurrentLevel++;
    EnemiesLeft = Level.EnemiesCount;
  }

  private LevelSettings.Level Level
  {
    get { return _levels.Levels[CurrentLevel]; }
  }
}
