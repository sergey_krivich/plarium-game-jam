﻿using UnityEngine;

public class DestroyInSeconds : MonoBehaviour
{
  public float Time = 2f;

	void Start () {
		Destroy(gameObject, Time);
	}
	
}
