﻿using UnityEngine;

public class ParticlesHolder : MonoBehaviour
{
  public GameObject EnemyDead;
  public GameObject EnemyEaten;


  public GameObject ChangeForm;
  public GameObject BloodHit;
  public GameObject BloodContinious;
  public GameObject SpawnEnemy;


  public static ParticlesHolder I;
  public void Awake()
  {
    I = this;
  }
}
